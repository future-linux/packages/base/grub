# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=grub
pkgver=2.06
unifont_ver=14.0.04
pkgrel=1
pkgdesc="GNU GRand Unified Bootloader (2)"
arch=('x86_64')
url="https://www.gnu.org/software/grub/"
license=('GPL3')
groups=('base')
depends=('efibootmgr')
makedepends=('freetype' 'python')
options=('!makeflags')
source=(https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.xz
    https://unifoundry.com/pub/unifont/unifont-${unifont_ver}/font-builds/unifont-${unifont_ver}.pcf.gz)
sha256sums=(b79ea44af91b93d17cd3fe80bdae6ed43770678a9a5ae192ccea803ebb657ee1
    439951ee15f8f8ff90f5f5438f0f46b4b1aa508b25b0a3969e7f237bce5f1fed)

prepare() {
    cd ${pkgname}-${pkgver}

    sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i configure.ac

    gzip -cd ${srcdir}/unifont-${unifont_ver}.pcf.gz > unifont.bdf

    sed -i 's/${PYTHON:=python}/${PYTHON:=python3}/g' autogen.sh
}

build() {
    cd ${pkgname}-${pkgver}

    unset {C,CPP,CXX,LD}FLAGS

    ./autogen.sh

    ${configure}             \
        --sysconfdir=/etc    \
        --disable-efiemu     \
        --enable-grub-mkfont \
        --with-platform=efi  \
        --target=x86_64      \
        --disable-werror

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} bashcompletiondir=/usr/share/bash-completion/completions install

}
